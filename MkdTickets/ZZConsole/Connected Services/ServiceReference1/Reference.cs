﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZZConsole.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IMkdService")]
    public interface IMkdService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMkdService/Check", ReplyAction="http://tempuri.org/IMkdService/CheckResponse")]
        string Check(string code);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMkdService/AddTicket", ReplyAction="http://tempuri.org/IMkdService/AddTicketResponse")]
        bool AddTicket(int type, string code, bool isTest);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMkdService/GetReport", ReplyAction="http://tempuri.org/IMkdService/GetReportResponse")]
        string GetReport(System.DateTime date);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMkdServiceChannel : ZZConsole.ServiceReference1.IMkdService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MkdServiceClient : System.ServiceModel.ClientBase<ZZConsole.ServiceReference1.IMkdService>, ZZConsole.ServiceReference1.IMkdService {
        
        public MkdServiceClient() {
        }
        
        public MkdServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MkdServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MkdServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MkdServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string Check(string code) {
            return base.Channel.Check(code);
        }
        
        public bool AddTicket(int type, string code, bool isTest) {
            return base.Channel.AddTicket(type, code, isTest);
        }
        
        public string GetReport(System.DateTime date) {
            return base.Channel.GetReport(date);
        }
    }
}
