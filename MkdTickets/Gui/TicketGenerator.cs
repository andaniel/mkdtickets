﻿
using MkdTickets.Printer.Markup;
using Printer.Markup;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
    class TicketGenerator
    {
        public static Document GetReport(int fullDayTicketsCount, int halfDayTicketsCount, int tourTicketsCount, int childFullDayTicketsCount, int childHalfDayTicketsCount, int childTourTicketsCount, string date)
        {
            var doc = new Document();
            doc.AlbumList = false;

            var fontBig = new Font { Size = FontSize.Big, Style = FontStyle.Bold };
            doc.Fonts.Add("big", fontBig);

            var fontNormal = new Font { Size = FontSize.Normal, Style = FontStyle.None };
            doc.Fonts.Add("normal", fontNormal);

            var rect = new Rectangle();
            rect.X = 50;
            rect.Y = 0;
            rect.Width = 220;
            rect.Height = 70;
            

            var upperText = new Text();
            upperText.Font = "big";
            
            upperText.Content.Add(new Cell() { Alignment = CellAlignment.Left, Content = "Дата: " + date + "\r\n" });
            //doc.Elements.Add(upperText);

            rect.Text = upperText;

            var rect1 = new Rectangle();
            rect1.X = 50;
            rect1.Y = 70;
            rect1.Width = 220;
            rect1.Height = 100;

            var fullDayText = new Text();
            fullDayText.Font = "normal";
            fullDayText.Content.Add(new Cell() { Alignment = CellAlignment.Left, Content = "Дневных абонементов: " + fullDayTicketsCount + "\r\nПолудневных абонементов: " + halfDayTicketsCount + "\r\nТуристических билетов: " + tourTicketsCount + "\r\nДетских Дневных : " + childFullDayTicketsCount + "\r\nДетских Полудневных: " + childHalfDayTicketsCount + "\r\nДетских Туристических: " + childTourTicketsCount + "\r\n" });
            
            rect1.Text = fullDayText;
            
            doc.Elements.Add(rect);
            doc.Elements.Add(rect1);
            return doc;
        }

        public static Document GetTicket(string dayStr, string code)
        {
            var doc = new Document();
            doc.AlbumList = false;

            var fontBig = new Font { Size = FontSize.Big, Style = FontStyle.Bold };
            doc.Fonts.Add("big", fontBig);

            var fontNormal = new Font { Size = FontSize.Normal, Style = FontStyle.None };
            doc.Fonts.Add("normal", fontNormal);

            var txt = new Text { Font = "big" };
            var cell = new Cell() { Alignment = CellAlignment.Left };
            var upper = string.Empty;// "АБОНЕМЕНТ\r\n";
            upper += dayStr;// + "\r\n";
            upper += DateTime.Now.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture) + "\r\n\r\n";


            cell.Content = upper;
            txt.Content.Add(cell);

            var img = new ImageMarkup();
            img.Source = LogoWorker.LogoPath;
            img.X = 50;
            img.Y = 0;
            img.Height = 100;
            img.Width = 210;

            var rect = new Rectangle();
            rect.X = 50;
            rect.Y = 100;
            rect.Text = txt;
            rect.Width = 230;
            rect.Height = 200;

            doc.Elements.Add(img);
            doc.Elements.Add(rect);

            var txtFooter = new Text { Font = "normal" };
            var cellFooter = new Cell() { Alignment = CellAlignment.Left };
            cellFooter.Content = "Сохраняйте до конца катания\r\n\r\n";
            txtFooter.Content.Add(cellFooter);

            var footer = new Rectangle();
            footer.X = 50;
            footer.Y = 200;
            footer.Height = 50;
            footer.Width = 230;

            //footer.ShowBoard = true;
            footer.Text = txtFooter;
            doc.Elements.Add(footer);

            //BarcodeGenerator barcodeGenerator = new BarcodeGenerator(EncodeTypes.Code128, code);
            //var barcode = barcodeGenerator.GenerateBarCodeImage();

            BarcodeLib.Barcode gen = new BarcodeLib.Barcode();
            var barcode = gen.Encode(BarcodeLib.TYPE.CODE128, code);

            var imgCode = new ImageMarkup();
            imgCode.Bitmap = barcode;
            imgCode.X = 50;
            imgCode.Y = 220;
            imgCode.Height = 100;//barcode.Height;
            imgCode.Width = 210;//barcode.Width;

            doc.Elements.Add(imgCode);
            return doc;
        }
    }
}
