﻿using Core.DataLayer;
using MkdTickets.Device.Printer.Windows;
using MkdTickets.Printer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Gui
{
    class MainWindowViewModel :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        static MkdServiceReference.MkdServiceClient client = new MkdServiceReference.MkdServiceClient();

        private void LogoWorker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var path = "C:\\Debug\\logos\\" + LogoWorker.LogoId.ToString() + ".png";
            var uri = new Uri(path);
            var img = new BitmapImage(uri);
            img.Freeze();
            Dispatcher.CurrentDispatcher.Invoke(() => { Logo = img; });
        }

        public MainWindowViewModel()
        {
            FullDayCommand = new RelayCommand(FullDayCommandExecuted);
            ReportCommand = new RelayCommand(ReportCommandExecuted);
            LogoWorker.PropertyChanged += LogoWorker_PropertyChanged;
        }

        public static void FullDayCommandExecuted(object obj)
        {   
            var code = CodeGenertor.GetCode();

            //DbContext.InsertTicket((TicketType)obj, code,0);
            
            client.AddTicket((int)obj, code, false);
            PrinterWorker.PrintTicket((TicketType)obj, code);
        }

        public static void ReportCommandExecuted(object obj)
        {
            var serviceWindow = new ServiceWindow();
            serviceWindow.ShowDialog();
        }

        public ICommand FullDayCommand { get; set; }
        
        public ICommand ReportCommand { get; set; }
        

        private BitmapImage logo;
        public BitmapImage Logo
        {
            get
            {
                return logo;
            }
            set
            {
                logo = value;
                OnPropertyChanged();
            }
        }
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
