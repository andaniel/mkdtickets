﻿using Core.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
    class LogoWorker
    {
        private static int logoId;
        public static int LogoId { get
            {
                return logoId;
            }
            set
            {
                logoId = value;
                OnPropertyChanged();
            } }
        public static string LogoPath
        {
            get
            {
                return "C:\\Debug\\logos\\" + LogoWorker.LogoId.ToString() + ".png";
            }
        }
        private static void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(null, new PropertyChangedEventArgs(propertyName));
        }
        public static event PropertyChangedEventHandler PropertyChanged;

        public static void Init()
        {
            var lastDate = DbContext.GetLogoDate();
            if(lastDate.Date != DateTime.Now.Date)
            {
                var random = new Random(Environment.TickCount);
                var id = DbContext.GetLogoId();
                int newId = 0;
                do
                {
                    newId = random.Next(1, 6);
                } while (id == newId);

                    DbContext.SaveLogoId(newId);
                LogoId = newId;
            }else
            {
                LogoId = DbContext.GetLogoId();
            }
        }
    }
}
