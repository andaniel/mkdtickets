﻿
using Core.DataLayer;
using MkdTickets.Device.Printer.Windows;
using MkdTickets.Printer;
using Printer.Markup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
    class PrinterWorker
    {
        static MkdTickets.Device.Printer.Windows.Device printer = null;

        static PrinterWorker()
        {
            DeviceConfiguration printerConfig = new DeviceConfiguration();
            PrinterListener printerListener = new PrinterListener();
            //printerConfig.PrinterName = "CUSTOM VKP80 II";
            printerConfig.PrinterName = "PDF24";
            printer = new MkdTickets.Device.Printer.Windows.Device(printerConfig, printerListener);
        }

        public static void PrintTicket(TicketType type, string code)
        {
            switch (type)
            {
                case TicketType.Full:
                    printer.Print(TicketGenerator.GetTicket("АБОНЕМЕНТ\r\nНА ДЕНЬ\r\n",code));
                    break;
                case TicketType.Half:
                    printer.Print(TicketGenerator.GetTicket("АБОНЕМЕНТ\r\nНА ПОЛДНЯ\r\n", code));
                    break;
                case TicketType.Tourist:
                    printer.Print(TicketGenerator.GetTicket("БИЛЕТ\r\nТУРИСТ\r\n", code));
                    break;
                case TicketType.ChildFull:
                    printer.Print(TicketGenerator.GetTicket("ДЕТСКИЙ\r\nНА ДЕНЬ\r\n", code));
                    break;
                case TicketType.ChildHalf:
                    printer.Print(TicketGenerator.GetTicket("ДЕТСКИЙ\r\nНА ПОЛДНЯ\r\n", code));
                    break;
                case TicketType.ChildTourist:
                    printer.Print(TicketGenerator.GetTicket("ДЕТСКИЙ\r\nТУРИСТ\r\n", code));
                    break;
                default:
                    break;
            }
        }

        public static void PrintReport(Document doc)
        {
            printer.Print(doc);
        }
    }
}
