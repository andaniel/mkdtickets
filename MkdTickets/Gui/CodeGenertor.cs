﻿using Core.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
    class CodeGenertor
    {
        private static Random rnd = new Random(Environment.TickCount);
        public static string GetCode()
        {   
            var code = 0;
            var checkResult = 1;
            do
            {
                code = rnd.Next(1000000, 9999999);
                checkResult = DbContext.CodeTicketCount(code.ToString());

            } while (checkResult == 1);
            return code.ToString();
        }
    }
}
