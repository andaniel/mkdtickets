﻿using Core.DataLayer;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Gui
{
    class ServiceWindowViewModel
    {
        static Action closeAct = null;
        public ServiceWindowViewModel(Action close)
        {
            closeAct = close;
            ReportCommand = new RelayCommand(ReportCommandExecuted);
            CloseCommand = new RelayCommand(CloseCommandExecuted);
            PrinterTestCommand = new RelayCommand(PrinterTestCommandExecuted);
        }
        public ICommand ReportCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand PrinterTestCommand { get; set; }

        public static void ReportCommandExecuted(object obj)
        {
            var start = DateTime.Now.Date;
            var stop = DateTime.Now.AddDays(1).Date;
            var fullDayTickets = DbContext.GetTicketsCount(TicketType.Full,start,stop);
            var halfDayTickets = DbContext.GetTicketsCount(TicketType.Half, start, stop);
            var tourTickets = DbContext.GetTicketsCount(TicketType.Tourist, start, stop);
            var childfullDayTickets = DbContext.GetTicketsCount(TicketType.ChildFull, start, stop);
            var childhalfDayTickets = DbContext.GetTicketsCount(TicketType.ChildHalf, start, stop);
            var childtourTickets = DbContext.GetTicketsCount(TicketType.ChildTourist, start, stop);
            PrinterWorker.PrintReport(TicketGenerator.GetReport(fullDayTickets, halfDayTickets, tourTickets,childfullDayTickets,childhalfDayTickets,childtourTickets, DateTime.Now.ToString("dd.MM.yyyy")));
        }
        public static void CloseCommandExecuted(object obj)
        {
            closeAct();
        }
        public static void PrinterTestCommandExecuted(object obj)
        {
            var code = CodeGenertor.GetCode();
            //DbContext.InsertTicket((TicketType)obj, code, 1);
            MkdServiceReference.MkdServiceClient client = new MkdServiceReference.MkdServiceClient();
            client.AddTicket((int)obj, code, true);
            PrinterWorker.PrintTicket(TicketType.Full, code);
        }

    }
}
