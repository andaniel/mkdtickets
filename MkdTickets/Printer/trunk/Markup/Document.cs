﻿using MkdTickets.Printer.Markup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace Printer.Markup
{
    [ContentProperty("Elements")]
    public class Document
    {
        private bool albumList = false;
        public bool AlbumList
        {
            get
            {
                return albumList;
            }
            set
            {
                albumList = value;
            }
        }

        private DocumentType type = DocumentType.Information;
        private decimal sum = 0;

        private Dictionary<string, Font> fonts = new Dictionary<string, Font>();
        private List<IDocumentElement> elements = new List<IDocumentElement>();

        public DocumentType Type
        {
            get
            {
                return this.type;
            }

            set
            {
                this.type = value;
            }
        }

        public decimal Sum
        {
            get
            {
                return this.sum;
            }

            set
            {
                this.sum = value;
            }
        }

        public string SumString
        {
            set
            {
                this.sum = Decimal.Parse(value);
            }
        }

        public Dictionary<string, Font> Fonts
        {
            get
            {
                return this.fonts;
            }
        }

        public List<IDocumentElement> Elements
        {
            get
            {
                return this.elements;
            }
        }
    }
}
