﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public class Rectangle : IDocumentElement
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public object Text { get; set; }
        public bool ShowBoard { get; set; }
        public string Coef { get; set; } = "1";
    }
}
