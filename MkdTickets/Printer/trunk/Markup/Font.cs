﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public class Font
    {
        private FontSize size = FontSize.Normal;
        private FontStyle style = FontStyle.None;

        public FontSize Size
        {
            get
            {
                return this.size;
            }

            set
            {
                this.size = value;
            }
        }

        public FontStyle Style
        {
            get
            {
                return this.style;
            }

            set
            {
                this.style = value;
            }
        }
    }
}
