﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public class ImageMarkup : IDocumentElement
    {
        public string Source { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        private Image bitmap;
        public Image Bitmap
        {
            get
            {
                if (bitmap == null)
                {
                    return Image.FromFile(Source);
                }
                else
                {
                    return bitmap;
                }
            }
            set
            {
                bitmap = value;
            }
        }
    }
}
