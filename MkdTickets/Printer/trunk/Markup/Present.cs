﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public class Present : IDocumentCondition
    {
        private PrinterFeature features = PrinterFeature.None;

        public bool Calculate(PrinterFeature features)
        {
            return (this.features & features) == this.features;
        }

        public PrinterFeature Features
        {
            get
            {
                return this.features;
            }

            set
            {
                this.features = value;
            }
        }
    }
}
