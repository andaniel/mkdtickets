﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public enum FontSize
    {
        Small,
        Normal,
        Big,
        VerySmall
    }
}
