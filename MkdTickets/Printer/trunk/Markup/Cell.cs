﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace MkdTickets.Printer.Markup
{
    [ContentProperty("Content")]
    public class Cell
    {
        private double width;
        private CellAlignment alignment;
        private object content = String.Empty;

        // для общности, ни одна нормальная реализация не должна использовать его так тупо :)
        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();
            foreach (object o in content.ToString())
                buffer.Append(o.ToString());

            return buffer.ToString();
        }

        public String Height { get; set; }
        public double Width
        {
            get
            {
                return this.width;
            }

            set
            {
                this.width = value;
            }
        }

        public CellAlignment Alignment
        {
            get
            {
                return this.alignment;
            }

            set
            {
                this.alignment = value;
            }
        }

        public object Content
        {
            get
            {
                return this.content;
            }

            set
            {
                this.content = value;
            }
        }
    }
}
