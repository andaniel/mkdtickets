﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    [Flags]
    public enum PrinterFeature
    {
        None = 0,
        FreeFormPos = 0x01,
        Imaging = 0x02,
    }
}
