﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    [Flags]
    public enum FontStyle
    {
        None = 0,
        Bold = 1,
        Italic = 2,
        Underline = 4,
        Monospace = 8,
    }
}
