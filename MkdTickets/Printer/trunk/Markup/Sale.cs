﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public class Sale
    {
        private decimal price;

        public override string ToString()
        {
            return this.price.ToString();
        }

        public decimal Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
            }
        }
    }
}
