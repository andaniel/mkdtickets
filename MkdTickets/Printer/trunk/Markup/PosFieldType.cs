﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public enum PosFieldType
    {
        SerialNumber,
        DocumentNumber,
        Date,
        Time,
        INN,
        Operator,
        Sum,
    }
}
