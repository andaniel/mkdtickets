﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public enum CellAlignment
    {
        Left,
        Center,
        Right
    }
}
