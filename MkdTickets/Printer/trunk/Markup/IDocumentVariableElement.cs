﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Printer.Markup
{
    public interface IDocumentVariableElement : IDocumentElement
    {
        List<IDocumentElement> Resolve(PrinterFeature features);
    }
}
