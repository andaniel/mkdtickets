﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace MkdTickets.Printer.Markup
{
    [ContentProperty("Content")]
    public class Text : IDocumentElement
    {
        private string font;
        private List<object> content = new List<object>();

        public string Font
        {
            get
            {
                return this.font;
            }

            set
            {
                this.font = value;
            }
        }

        public List<object> Content
        {
            get
            {
                return this.content;
            }
        }
    }
}
