﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace MkdTickets.Printer.Markup
{
    public class If : IDocumentVariableElement
    {
        private IDocumentCondition condition;
        private List<IDocumentElement> _true = new List<IDocumentElement>();
        private List<IDocumentElement> _false = new List<IDocumentElement>();

        public List<IDocumentElement> Resolve(PrinterFeature features)
        {
            if (this.condition.Calculate(features) == true)
            {
                return this._true;
            }
            else
            {
                return this._false;
            }
        }

        public IDocumentCondition Condition
        {
            get
            {
                return this.condition;
            }

            set
            {
                this.condition = value;
            }
        }

        public List<IDocumentElement> True
        {
            get
            {
                return this._true;
            }
        }

        public List<IDocumentElement> False
        {
            get
            {
                return this._false;
            }
        }
    }
}
