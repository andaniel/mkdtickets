﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtSoft.Presentation
{
    public class BarcodeScannedPopper : ILocalPopper
    {
        private string barcode;

        public BarcodeScannedPopper(string barcode)
        {
            this.barcode = barcode;
        }

        public string Barcode
        {
            get
            {
                return this.barcode;
            }
        }
    }
}
