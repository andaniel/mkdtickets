﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace ExtSoft.Presentation
{
    public static class InvariantConverter
    {
        public static decimal ToDecimal(object o)
        {
            if (o == null)
            {
                return 0;
            }
            else if (o is decimal)
            {
                return (decimal)o;
            }
            else if (o is float)
            {
                float f = (float)o;
                return (decimal)f;
            }
            else if (o is double)
            {
                double d = (double)o;
                return (decimal)d;
            }
            else if (o is long)
            {
                long l = (long)o;
                return (decimal)l;
            }
            else if (o is ulong)
            {
                ulong u = (ulong)o;
                return (decimal)u;
            }
            else if (o is int)
            {
                int i = (int)o;
                return (decimal)i;
            }
            else if (o is uint)
            {
                uint u = (uint)o;
                return (decimal)u;
            }
            else if (o is short)
            {
                short s = (short)o;
                return (decimal)s;
            }
            else if (o is ushort)
            {
                ushort u = (ushort)o;
                return (decimal)u;
            }
            else if (o is sbyte)
            {
                sbyte s = (sbyte)o;
                return (decimal)s;
            }
            else if (o is byte)
            {
                byte b = (byte)o;
                return (decimal)b;
            }
            else if (o is bool)
            {
                bool b = (bool)o;
                return b ? 1 : 0;
            }
            else
            {
                string rawString = Convert.ToString(o);
                string preparedString = rawString.Replace(NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator, NumberFormatInfo.InvariantInfo.CurrencyDecimalSeparator);

                decimal d;
                if (Decimal.TryParse(preparedString, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out d))
                {
                    return d;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
