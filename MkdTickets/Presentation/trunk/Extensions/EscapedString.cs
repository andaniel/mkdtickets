﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace MkdTickets.Presentation.Extensions
{
    [ContentProperty("Text")]
    public class EscapedString : MarkupExtension
    {
        public string Text { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this.Text.Replace("\\n ", System.Environment.NewLine).Replace("\\n", System.Environment.NewLine);
        }
    }
}
