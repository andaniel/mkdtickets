﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace MkdTickets.Presentation.Extensions
{
    public class NewLine : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return "\r\n";
        }
    }
}
