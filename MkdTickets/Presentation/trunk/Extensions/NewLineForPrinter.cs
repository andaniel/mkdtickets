﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace MkdTickets.Presentation.Extensions
{
    public class NewLineForPrinter : MarkupExtension
    {
        public static string NewLineView = "(x(x_(x_x(О_о)x_x)_x)x)";

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return NewLineView;
        }
    }
}
