﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class CollectionConstrain : AbstractConstrain
    {
        public bool IsNotNull { get; set; }
        private int count = Int32.MinValue;
        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                this.count = value;
            }
        }

        public override string GenerateCode(Class c)
        {
            StringBuilder sB = new StringBuilder();
            int constrainCounter = 0;
            if (this.IsNotNull == true)
                constrainCounter++;
            if (this.Count != Int32.MinValue)
                constrainCounter++;
            Property p = (from pr in c.Properties
                          where pr.Name == this.TargetName
                          select pr).Single();
            if (constrainCounter > 1)
                sB.Append("(");
            if (this.IsNotNull == true)
                sB.Append("(this." + p.Name + ".Count>0)");
            if (this.Count != Int32.MinValue)
            {
                if (sB.Length != 0)
                    sB.Append(" && ");
                sB.Append("(this." + p.Name + ".Count==" + this.Count.ToString() + ")");
            }
            if (sB.Length == 0)
                sB.Append("(true)");
            if (constrainCounter > 1)
                sB.Append(")");
            return sB.ToString();
        }
    }
}
