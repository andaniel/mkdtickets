﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class CollectionItemsConstrain : AbstractConstrain
    {
        public override string GenerateCode(Class c)
        {
            StringBuilder sB = new StringBuilder();
            Property p = (from pr in c.Properties
                          where pr.Name == this.TargetName
                          select pr).Single();
            //sB.Append("(CollectionItemsConstrain.ValidateList(this." + p.Name + ".ConvertAll(objectCollection => (object)objectCollection)))");
            sB.Append("(CollectionItemsConstrain.ValidateList(this." + p.Name + "))");
            return sB.ToString();
        }

        public static bool ValidateList(ICollection collection)
        {
            foreach (object element in collection)
            {
                IValidatable validatableElement = (IValidatable)element;
                if (!validatableElement.Validate())
                    return false;
            }
            return true;
            //bool returnValue = false;
            //if (collection.All(element => element is IValidatable))
            //    returnValue = collection.All(element =>
            //        {
            //            IValidatable iValidateElement = (IValidatable)element;
            //            return iValidateElement.Validate();
            //        }
            //    );
            //return returnValue;
        }
    }
}
