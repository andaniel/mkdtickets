﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class StringConstrain : AbstractConstrain
    {
        public bool IsNotNull { get; set; }

        private int minlength = Int32.MinValue;

        public int MinLength
        {
            get
            {
                return this.minlength;
            }
            set
            {
                this.minlength = value;
            }
        }

        private int maxLength = Int32.MaxValue;
        public int MaxLength
        {
            get
            {
                return this.maxLength;
            }
            set
            {
                this.maxLength = value;
            }
        }

        public override string GenerateCode(Class c)
        {
            int constrainCounter = 0;
            if (this.IsNotNull == true)
                constrainCounter++;
            if (this.MinLength != Int32.MinValue)
                constrainCounter++;
            if (this.MaxLength != Int32.MaxValue)
                constrainCounter++;
            Property p = (from pr in c.Properties
                          where pr.Name == this.TargetName
                          select pr).Single();
            StringBuilder sB = new StringBuilder();
            if (constrainCounter > 1)
                sB.Append("(");
            if (this.IsNotNull == true)
            {
                sB.Append("(!String.IsNullOrEmpty(this." + p.Name + "))");
            }

            if (this.MinLength != Int32.MinValue)
            {
                if (sB.Length != 0)
                    sB.Append(" && ");

                sB.Append("(this." + p.Name + ".Length >= " + this.MinLength + ")");
            }

            if (this.MaxLength != Int32.MaxValue)
            {
                if (sB.Length != 0)
                    sB.Append(" && ");

                sB.Append("(this." + p.Name + ".Length <= " + this.MaxLength + ")");
            }

            if (sB.Length == 0)
                sB.Append("(true)");
            if (constrainCounter > 1)
                sB.Append(")");
            return sB.ToString();
        }
    }
}
