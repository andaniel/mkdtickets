﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public interface IValidatable
    {
        bool Validate();
        string ConstrainString { get; }
    }
}
