﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class ObjectConstrain : AbstractConstrain
    {
        public bool IsNotNull { get; set; }
        public bool ValidateIValidateObject {get; set;}

        public override string GenerateCode(Class c)
        {
            int constrainCounter = 0;
            if (this.IsNotNull == true)
                constrainCounter++;
            if (this.ValidateIValidateObject == true)
                constrainCounter++;
            Property p = (from pr in c.Properties
                          where pr.Name == this.TargetName
                          select pr).Single();
            StringBuilder sB = new StringBuilder();
            if (constrainCounter > 1)
                sB.Append("(");
            if (this.IsNotNull == true)
            {
                sB.Append("(!(this." + p.Name + " == null))");
            }
            if (this.ValidateIValidateObject == true)
            {
                if (sB.Length != 0)
                    sB.Append(" && ");

                sB.Append("(ObjectConstrain.ValidateIValidate(this." + p.Name + "))");
            }
            if (sB.Length == 0)
                sB.Append("(true)");
            if (constrainCounter > 1)
                sB.Append(")");
            return sB.ToString();
        }
        public static bool ValidateIValidate(object obj)
        {
            bool returnValue = false;
            if (obj is IValidatable)
            {
                IValidatable val = (IValidatable)obj;
                returnValue = val.Validate();
            }
            return returnValue;
        }
    }
}
