﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Sox;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class Class
    {
        public string Name { get; set; }
        public string Extends { get; set; }
        public bool IsAtomic { get; set; }

        //[DefaultValue(null), UniqueType]
        public Properties Properties { get; set; }

        //[UniqueType]
        public Constrains Constrains { get; set; }
    }
}
