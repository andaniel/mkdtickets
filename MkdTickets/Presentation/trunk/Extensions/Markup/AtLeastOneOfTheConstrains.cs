﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
//using Sox;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class AtLeastOneOfTheConstrains : List<IConstrain>, IConstrain
    {
        public string GenerateCode(Class c)
        {
            StringBuilder sB = new StringBuilder();
            if (this.Count > 1)
                sB.Append("(");
            foreach (IConstrain iConstrain in this)
            {
                if (sB.Length > 1)
                    sB.Append(" || ");
                sB.Append(iConstrain.GenerateCode(c));
            }
            if (this.Count > 1)
                sB.Append(")");
            return sB.ToString();
        }
    }
}
