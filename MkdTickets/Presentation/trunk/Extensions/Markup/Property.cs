﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class Property
    {
        internal static readonly object UndefinedValue = new object();

        public string Type { get; set; }
        public string Name { get; set; }

        public object DefaultValue { get; set; }
        public bool IsUniqueType { get; set; }
        public bool IsPasswordText { get; set; }

        public Property()
        {
            this.Type = "object";
            this.DefaultValue = UndefinedValue;
        }
    }
}
