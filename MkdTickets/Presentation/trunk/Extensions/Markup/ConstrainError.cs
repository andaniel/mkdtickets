﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Presentation.Extensions.Markup
{
    public class ConstrainError
    {
        public string Message { get; set; }
        public string ErrorObjectName { get; set; }
    }
}
