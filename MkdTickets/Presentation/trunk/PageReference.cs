using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace ExtSoft.Presentation
{
    public class PageReference
    {
        private PageReferenceType type;

        private string file;
        private Page page;

        private PageReference() { }

        public static PageReference FromFile(string file)
        {
            PageReference reference = new PageReference();
            reference.type = PageReferenceType.File;
            reference.file = file;

            return reference;
        }

        public static PageReference FromMemory(Page page)
        {
            PageReference reference = new PageReference();
            reference.type = PageReferenceType.Memory;
            reference.page = page;

            return reference;
        }

        public PageReferenceType Type
        {
            get
            {
                return this.type;
            }
        }

        public string File
        {
            get
            {
                return this.file;
            }
        }

        public Page Page
        {
            get
            {
                return this.page;
            }
        }
    }
}
