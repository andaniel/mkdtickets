﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MkdWcf.Entities
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime SellTime { get; set; }
        public int Type { get; set; }
        public string Code { get; set; }
        public bool IsTest { get; set; }

        public virtual ICollection<Passes> Passes { get; set; }
    }
}
