﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MkdWcf.Entities
{
    public class MkdDbContext:DbContext
    {
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Passes> Passes { get; set; }
    }
}
