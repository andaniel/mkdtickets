﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MkdWcf.Entities
{
    public class Passes
    {
        public int Id { get; set; }
        public DateTime PassTime { get; set; }
        public int TicketId { get; set; }
    }
}
