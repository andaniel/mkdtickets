﻿
using MkdWcf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MkdWcf
{
    class MkdService : IMkdService
    {
        MkdDbContext context = new MkdDbContext();

        public bool AddTicket(int type, string code, bool isTest)
        {
            //todo: проверка на дубликат билета по code
            
            context.Tickets.Add(new Ticket() { Code = code, IsTest = isTest, Type = type, SellTime=DateTime.Now });
            context.SaveChanges();
            return true;
        }

        public string Check(string code)
        {
            //todo: возвращать тип билета (детский, полный, пол дня, одноразовый)
            //todo: если билет одноразовый, то проверять сколько раз по нему прошли
            var tickets = context.Tickets.Where(x => x.Code == code).ToList();
            var ticket = tickets.FirstOrDefault(x => x.SellTime.Date == DateTime.Now.Date);
            if(ticket == null)
            {
                return "No Ticket";
            }
            var pass = ticket.Passes.OrderByDescending(x=>x.PassTime).FirstOrDefault();
            if(pass == null)
            {
                ticket.Passes.Add(new Passes() { PassTime = DateTime.Now });
                context.SaveChanges();
                return ticket.Type.ToString();
                
            }
            if((DateTime.Now-pass.PassTime)<TimeSpan.FromMinutes(5))
            {
                return "FastPass";
            }
            ticket.Passes.Add(new Passes() { PassTime = DateTime.Now });
            context.SaveChanges();
            return ticket.Type.ToString();
        }

        public string GetReport(DateTime date)
        {
            var start = date.Date;
            var stop = date.AddDays(1).Date;

            var context = new MkdDbContext();
            var fullDayTickets = context.Tickets.Where(x=>!x.IsTest && x.Type== (int)Core.DataLayer.TicketType.Full && x.SellTime>start&&x.SellTime<stop).Count();
            var halfDayTickets = context.Tickets.Where(x => !x.IsTest && x.Type == (int)Core.DataLayer.TicketType.Half && x.SellTime > start && x.SellTime < stop).Count();
            var tourTickets = context.Tickets.Where(x => !x.IsTest && x.Type == (int)Core.DataLayer.TicketType.Tourist && x.SellTime > start && x.SellTime < stop).Count();
            var childfullDayTickets = context.Tickets.Where(x => !x.IsTest && x.Type == (int)Core.DataLayer.TicketType.ChildFull && x.SellTime > start && x.SellTime < stop).Count();
            var childhalfDayTickets = context.Tickets.Where(x => !x.IsTest && x.Type == (int)Core.DataLayer.TicketType.ChildHalf && x.SellTime > start && x.SellTime < stop).Count();
            var childtourTickets = context.Tickets.Where(x => !x.IsTest && x.Type == (int)Core.DataLayer.TicketType.ChildTourist && x.SellTime > start && x.SellTime < stop).Count();

            var result = string.Format("Дата: {6}\r\n\r\nДневных абонементов: {0}\r\nПолудневных абонементов: {1}\r\nТуристических билетов: {2}\r\nДетских Дневных: {3}\r\nДетских Полудневных: {4}\r\nДетских Туристических: {5}",fullDayTickets,halfDayTickets,tourTickets,childfullDayTickets,childhalfDayTickets,childtourTickets,start.ToString("dd.MM.yyyy"));
            return result;
        }
    }
}
