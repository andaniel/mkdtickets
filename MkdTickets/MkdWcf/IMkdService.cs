﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MkdWcf
{
    
    [ServiceContract]
    public interface IMkdService
    {
        [OperationContract]
        string Check(string code);

        [OperationContract]
        bool AddTicket(int type, string code, bool isTest);
        [OperationContract]
        string GetReport(DateTime date);
    }
}
