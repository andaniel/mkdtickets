using System;
using System.Collections.Generic;
using System.Text;

namespace MkdTickets.Device
{
    public interface IDevice
    {
        void Close();

        IDeviceFactory Factory { get; }
    }
}
