﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Events
{
    public class MechanicalFailureEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Механическое повреждение устройства";
            }
        }
    }
}
