﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MkdTickets.Device.Events
{
    /// <summary>
    /// Класс устарел из-за логической ошибки: устройство может сигнализировать
    /// ошибку запуска до того, как менеджер устройств выдаст событие о запуске устройства и в
    /// таком случае устройство будет считаться работоспособным (ошибка).
    /// </summary>
    [Obsolete("Класс устарел из-за логической ошибки")]
    public class DeviceStartedEvent : IDeviceOperationalEvent, IDeviceManagementEvent
    {
        public string Message
        {
            get
            {
                return "Устройство запущено";
            }
        }
    }
}
