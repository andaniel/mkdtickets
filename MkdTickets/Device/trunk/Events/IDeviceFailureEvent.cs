﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Events
{
    public interface IDeviceFailureEvent : IDeviceEvent { }
}
