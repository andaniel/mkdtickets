﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Events
{
    public class CommunicationFailureEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Ошибка связи с устройством";
            }
        }
    }
}
