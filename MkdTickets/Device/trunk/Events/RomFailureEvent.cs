﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MkdTickets.Device.Events
{
    public class RomFailureEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Ошибка постоянной памяти устройства";
            }
        }
    }
}
