﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MkdTickets.Device.Events
{
    public class DeviceStartingEvent : IDeviceOperationalEvent, IDeviceManagementEvent
    {
        public string Message
        {
            get
            {
                return "Устройство запускается";
            }
        }
    }
}
