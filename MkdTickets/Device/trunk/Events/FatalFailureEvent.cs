﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MkdTickets.Device.Events
{
    public class FatalFailureEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Невосстановимая ошибка устройства";
            }
        }
    }
}
