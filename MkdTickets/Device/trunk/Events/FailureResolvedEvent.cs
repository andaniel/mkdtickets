﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Events
{
    public class FailureResolvedEvent : IDeviceOperationalEvent
    {
        public string Message
        {
            get
            {
                return "Ошибка устранена";
            }
        }
    }
}
