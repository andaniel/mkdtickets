﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MkdTickets.Device.Events
{
    public class DeviceNotStartedEvent : IDeviceFailureEvent, IDeviceManagementEvent
    {
        private string exception;

        public string Exception
        {
            get
            {
                return this.exception;
            }

            set
            {
                this.exception = value;
            }
        }

        public string Message
        {
            get
            {
                return "Ошибка запуска устройства: " + this.exception;
            }
        }
    }
}
