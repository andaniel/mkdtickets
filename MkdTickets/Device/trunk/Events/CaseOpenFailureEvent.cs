﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Events
{
    public class CaseOpenFailureEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Открыт корпус устройства";
            }
        }
    }
}
