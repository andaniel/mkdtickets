using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

//using MkdTickets.Presentation;
using MkdTickets.Core.Events;

namespace MkdTickets.Device
{
    public interface IDeviceFactory
    {
        IDevice Create(IDeviceConfiguration configuration, IOriginatedEventListener eventListener);
        //IPresenter Configure(IDeviceConfiguration configuration, DeviceConfiguredHandler configured);

        Type Type { get; }
        ImageSource Logo { get; }
        string Name { get; }
        string Description { get; }
        string ConfigurationSectionName { get; }
        IDeviceConfiguration DefaultConfiguration { get; }
    }
}
