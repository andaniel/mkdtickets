﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device
{
    public interface IDetectableDeviceFactory : IDeviceFactory
    {
        ICollection<IDeviceConfiguration> Detect(DeviceDetectProgressHandler progressHandler);
    }
}
