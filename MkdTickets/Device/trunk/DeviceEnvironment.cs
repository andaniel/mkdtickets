﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device
{
    public class DeviceEnvironment
    {
        private const string FailDirectory = "MigrationConflicts";

        private static DeviceEnvironment instance;

        private string basePath;
        private string dataPath;
        private string failPath;

        private DeviceEnvironment(string basePath, string dataPath)
        {
            this.basePath = basePath;
            this.dataPath = dataPath;
            this.failPath = Path.Combine(this.dataPath, FailDirectory);
        }

        public string BasePath
        {
            get
            {
                return this.basePath;
            }
        }

        public string DataPath
        {
            get
            {
                return this.dataPath;
            }
        }

        public static void Initialize(string basePath, string dataPath)
        {
            if (instance == null)
            {
                instance = new DeviceEnvironment(basePath, dataPath);
            }
        }

        public void MigrateData(string toPath, string fromPath)
        {
            try
            {
                if (Directory.Exists(fromPath) == false)
                    return;

                string[] files = Directory.GetFiles(fromPath);

                foreach (string fromFile in files)
                {
                    string toFile = Path.Combine(toPath, Path.GetFileName(fromFile));
                    if (File.Exists(toFile))
                    {
                        toFile = Path.Combine(this.failPath,
                            DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH'.'mm'.'ss'.'fffffff") + "-" +
                            Path.GetFileName(fromFile));

                        CreateDirectory(this.failPath);
                    }

                    File.Move(fromFile, toFile);
                }

                files = Directory.GetFiles(fromPath);
                if (files.Length == 0)
                    Directory.Delete(fromPath);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Ошибка миграции папки '" + Path.GetDirectoryName(fromPath) + "'", ex);
            }
        }

        private void CreateDirectory(string path)
        {
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
        }

        public static DeviceEnvironment Instance
        {
            get
            {
                if (instance == null)
                    throw new NullReferenceException("Не было инициализировано окружение устройств");

                return instance;
            }
        }
    }
}
