using System;
using System.Collections.Generic;
using System.Text;

namespace MkdTickets.Device
{
    public interface IDeviceConfiguration
    {
        IDeviceFactory Factory { get; }

        bool Enabled { get; set; }
        Guid Id { get; set; }
    }
}
