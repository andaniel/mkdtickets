using System;
using System.Collections.Generic;
using System.Text;

namespace MkdTickets.Device
{
    public abstract class AbstractDeviceConfiguration : IDeviceConfiguration
    {
        private bool enabled;
        private Guid id;

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        public Guid Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }

        public abstract IDeviceFactory Factory { get; }
    }
}
