using System;
using System.Collections.Generic;
using System.Text;

namespace MkdTickets.Device
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class DeviceAttribute : Attribute
    {
        private readonly Type type;

        public DeviceAttribute(Type type)
        {
            this.type = type;
        }

        public Type Type
        {
            get
            {
                return this.type;
            }
        }

        /// <summary>
        /// ���� ������ ������������, �� �� ������ ���� ������ ��������
        /// </summary>
        public bool IsRequired
        { get; set; }
    }
}