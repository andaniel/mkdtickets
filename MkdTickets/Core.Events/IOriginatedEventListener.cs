﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Core.Events
{
    public interface IOriginatedEventListener
    {
        void Notify(IEvent e);
    }
}
