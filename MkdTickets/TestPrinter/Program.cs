﻿using MkdTickets.Printer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPrinter
{
    class Program
    {
        static void Main(string[] args)
        {

            MkdTickets.Device.Printer.Windows.DeviceConfiguration printerConfig = new MkdTickets.Device.Printer.Windows.DeviceConfiguration();
            printerConfig.PrinterName = "CUSTOM VKP80 II";
            PrinterListener printerListener = new PrinterListener();
            MkdTickets.Device.Printer.Windows.Device printer = new MkdTickets.Device.Printer.Windows.Device(printerConfig, printerListener);

            printer.Print(TicketGenerator.GetTicket());
        }
    }
}
