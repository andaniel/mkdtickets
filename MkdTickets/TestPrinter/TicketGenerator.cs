﻿using MkdTickets.Printer.Markup;
using Printer.Markup;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TestPrinter
{
    class TicketGenerator
    {
        public static Document GetTicket()
        {
            var doc = new Document();
            doc.AlbumList = false;

            var fontBig = new Font { Size = FontSize.Big, Style = FontStyle.Bold };
            doc.Fonts.Add("big", fontBig);

            var txt = new Text { Font = "big" };
            var cell = new Cell() { Alignment = CellAlignment.Left };
            var upper = "АБОНЕМЕНТ\r\n";
            upper += "НА ДЕНЬ\r\n";
            upper += DateTime.Now.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture)+"\r\n";
            //upper += "ООО \"МКД ЭЛЬБРУС\"";

            cell.Content = upper;
            txt.Content.Add(cell);
            //txt.Content.Add(upper);

            var img = new ImageMarkup();
            img.Source = "logoMkd.png";
            img.X = 50;
            img.Y = 0;
            img.Height = 300;
            img.Width = 628;

            var rect = new Rectangle();
            rect.X = 50;
            rect.Y = 100;
            rect.Text = txt;
            rect.Width =400;
            rect.Height = 200;
            //rect.Coef = "1";
            //rect.ShowBoard = true;

            doc.Elements.Add(img);
            doc.Elements.Add(rect);

            var footer = new Rectangle();
            footer.X = 0;
            footer.Y = 300;
            footer.Height = 2;
            footer.Width = 280;
            //footer.Coef = "1";
            footer.ShowBoard = true;
            
            doc.Elements.Add(footer);

            //doc.Elements.Add(txt);
            //Temp("АБОНЕМЕНТ", 0, 0, 100, 50, true, doc, CellAlignment.Center);
            //Temp("НА ДЕНЬ", 50, 0, 100, 50, true, doc, CellAlignment.Center);
            //Temp(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture), 0, 0, 100, 50, true, doc, CellAlignment.Center);
            return doc;
        }

        private static void Temp(string text, int x, int y, int w, int h, bool board, Document doc, CellAlignment aligment)
        {
            var txt = new Text { Font = "big" };

            var rec = new Rectangle { X = x, Y = y, Width = w, Height = h, ShowBoard = board, Coef = "1" };

            var cell = new Cell { Alignment = aligment, Content = text };

            txt.Content.Add(cell);
            rec.Text = txt;

            doc.Elements.Add(rec);
        }
    }
}
