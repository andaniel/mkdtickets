﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Printer
{
    public class PrinterDeviceException : ApplicationException
    {
        public PrinterDeviceException() { }
        public PrinterDeviceException(string message) : base(message) { }
        public PrinterDeviceException(string message, Exception innerException) : base(message, innerException) { }
    }
}
