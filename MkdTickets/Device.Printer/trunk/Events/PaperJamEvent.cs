﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MkdTickets.Core.Events;
using MkdTickets.Device.Events;

namespace MkdTickets.Device.Printer.Events
{
    //[EventCode(0x0303)]
    public class PaperJamEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get 
            {
                return "Замятие бумаги";
            }
        }
    }
}
