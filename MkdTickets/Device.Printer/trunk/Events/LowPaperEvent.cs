﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MkdTickets.Core.Events;
using MkdTickets.Device.Events;

namespace MkdTickets.Device.Printer.Events
{
    // не стоит пока использовать, так как не построен на "проблемах"
    // к тому же, ни одно из устройств пока не может корректно указать на данную проблему (((
    //[EventCode(0x0301)]
    public class LowPaperEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Бумага близка к концу";
            }
        }
    }
}
