﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MkdTickets.Core.Events;
using MkdTickets.Device.Events;

namespace MkdTickets.Device.Printer.Events
{
    //[EventCode(0x0304)]
    public class CutterFailureEvent : IDeviceFailureEvent
    {
        public string Message
        {
            get
            {
                return "Повреждение отрезчика бумаги";
            }
        }
    }
}
