﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MkdTickets.Device;
using Printer.Markup;

namespace MkdTickets.Device.Printer
{
    /// <summary>
    /// Принтер
    /// </summary>
    public interface IPrinterDevice : IDevice
    {
        void Print(Document document);
        bool Wait(int time, bool retract);
    }
}
