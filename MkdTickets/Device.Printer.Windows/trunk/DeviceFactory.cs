﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using MkdTickets.Presentation;
using System.IO;
using MkdTickets.Core.Events;
using System.Drawing.Printing;
using System.Threading;

namespace MkdTickets.Device.Printer.Windows
{
    public class DeviceFactory : IDetectableDeviceFactory
    {
        private static DeviceFactory instance;

        public static IDeviceFactory Instance
        {
            get
            {
                return instance != null ? instance : instance = new DeviceFactory();
            }
        }

        public IDevice Create(IDeviceConfiguration configuration, IOriginatedEventListener eventListener)
        {
            return new Device((DeviceConfiguration)configuration, eventListener);
        }

        //public IPresenter Configure(IDeviceConfiguration configuration, DeviceConfiguredHandler configured)
        //{
        //    return null;//new DeviceConfiguratorPresenter((DeviceConfiguration)configuration, configured);
        //}

        public ICollection<IDeviceConfiguration> Detect(DeviceDetectProgressHandler progressHandler)
        {
            List<IDeviceConfiguration> configurations = new List<IDeviceConfiguration>();
            System.Drawing.Printing.PrinterSettings.StringCollection printers = PrinterSettings.InstalledPrinters;

            int totalProgress = printers.Count;
            for (int i = 0; i < printers.Count; i++)
            {
                string printer = printers[i];
                MonitoringType monitoringType = MonitoringType.None;

                if (CustomMonitoring.Supports(printer))
                {
                    monitoringType = MonitoringType.Custom;
                }

                configurations.Add(new DeviceConfiguration { PrinterName = printer, MonitoringType = monitoringType });

                progressHandler(i + 1, totalProgress);
            }

            return configurations;
        }

        public Type Type
        {
            get
            {
                return typeof(Device);
            }
        }

        public ImageSource Logo
        {
            get
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = this.GetType().Assembly.GetManifestResourceStream(
                    "MkdTickets.Device.Printer.Windows.Resources.Logo.png");
                image.EndInit();
                return image;
            }
        }

        public string Name
        {
            get
            {
                return "Принтер Windows";
            }
        }

        public string Description
        {
            get
            {
                return "Стандартный принтер Windows";
            }
        }

        public string ConfigurationSectionName
        {
            get
            {
                return "Device.Printer.Windows";
            }
        }

        public IDeviceConfiguration DefaultConfiguration
        {
            get
            {
                return new DeviceConfiguration();
            }
        }
    }
}
