﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;

using MkdTickets.Core.Events;
using MkdTickets.Device.Events;
using MkdTickets.Device.Printer.Events;
using System.Windows;

namespace MkdTickets.Device.Printer.Windows
{
    public class CustomMonitoring : ThreadedMonitoring
    {
        private const int MaxWaitRetractTime = 13000;

        private const byte PaperStatusNotPresent = 0x01;
        private const byte PaperStatusNearEnd = 0x04;
        private const byte PaperStatusPaperInOutput = 0x20;

        private const byte UserStatusCoverOpen = 0x02;
        private const byte UserStatusSpooling = 0x04;
        private const byte UserStatusDragMotorOn = 0x08;

        private const byte RecoverableStatusTemrerature = 0x01;
        private const byte RecoverableStatusCom = 0x02;
        private const byte RecoverableStatusPowerSupply = 0x08;
        private const byte RecoverableStatusNak = 0x20;
        private const byte RecoverableStatusPaperJam = 0x40;

        private const byte IrrecoverableStatusCutter = 0x01;
        private const byte IrrecoverableStatusRam = 0x04;
        private const byte IrrecoverableStatusEeprom = 0x08;
        private const byte IrrecoverableStatusFlash = 0x40;

        [DllImport("CeSmLm.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint CePrnInitCeUsbSI(ref uint error);

        [DllImport("CeSmLm.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern uint CePrnGetInterfaceNumUsb(string name, ref int device);

        [DllImport("CeSmLm.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint CePrnGetStsUsb(int device, ref uint status, ref uint error);

        private IOriginatedEventListener eventListener;
        private int deviceId;
        private CustomFailureType failure = CustomFailureType.None;

        private ManualResetEvent paperInOutputEvent = new ManualResetEvent(false);

        private int stateId = 0;

        public CustomMonitoring(IOriginatedEventListener eventListener, string printerName)
        {
            this.eventListener = eventListener;

            uint error = 0;
            uint count = CePrnInitCeUsbSI(ref error);

            if (error != 0)
                throw new ArgumentException("Ошибка инициализации библиотеки статуса принтера Custom (" + error + ")");

            uint errInt = CePrnGetInterfaceNumUsb(printerName, ref this.deviceId);
            if (errInt != 0)
                throw new ArgumentException("Принтер с таким именем не является принтером Custom");

            Start();
        }

        public override void MonitoringLoop()
        {
            while (this.running)
            {
                CustomFailureType failure;

                uint error = 0;
                uint status = 0;
                CePrnGetStsUsb(this.deviceId, ref status, ref error);

                byte paperStatus = (byte)(status & 0xFF);
                byte userStatus = (byte)((status & 0xFF00) >> 8);
                byte recoverStatus = (byte)((status & 0xFF0000) >> 16);
                byte irrecoverableStatus = (byte)((status & 0xFF000000) >> 24);

                if ((paperStatus & PaperStatusPaperInOutput) != 0 ||
                    (userStatus & (UserStatusSpooling | UserStatusDragMotorOn)) != 0)
                {
                    this.paperInOutputEvent.Reset();
                }
                else
                {
                    this.paperInOutputEvent.Set();
                }

                if (error != 0)
                {
                    failure = CustomFailureType.Connection;
                }
                else if ((paperStatus & PaperStatusNotPresent) != 0)
                {
                    failure = CustomFailureType.OutOfPaper;
                }
                else if ((userStatus & UserStatusCoverOpen) != 0)
                {
                    failure = CustomFailureType.CoverOpen;
                }
                else if ((recoverStatus & RecoverableStatusTemrerature) != 0)
                {
                    failure = CustomFailureType.Overheating;
                }
                else if ((recoverStatus & RecoverableStatusCom) != 0)
                {
                    failure = CustomFailureType.Connection;
                }
                else if ((recoverStatus & RecoverableStatusPowerSupply) != 0)
                {
                    failure = CustomFailureType.PowerSupply;
                }
                else if ((recoverStatus & RecoverableStatusNak) != 0)
                {
                    failure = CustomFailureType.Connection;
                }
                else if ((recoverStatus & RecoverableStatusPaperJam) != 0)
                {
                    failure = CustomFailureType.PaperJam;
                }
                else if ((irrecoverableStatus & IrrecoverableStatusCutter) != 0)
                {
                    failure = CustomFailureType.Cutter;
                }
                else if ((irrecoverableStatus & IrrecoverableStatusRam) != 0)
                {
                    failure = CustomFailureType.Ram;
                }
                else if ((irrecoverableStatus & IrrecoverableStatusEeprom) != 0)
                {
                    failure = CustomFailureType.Rom;
                }
                else if ((irrecoverableStatus & IrrecoverableStatusFlash) != 0)
                {
                    failure = CustomFailureType.Rom;
                }
                else
                {
                    failure = CustomFailureType.None;
                }

                if (failure != this.failure)
                {
                    this.failure = failure;

                    switch (failure)
                    {
                        case CustomFailureType.None:
                            this.eventListener.Notify(new FailureResolvedEvent());
                            break;

                        case CustomFailureType.Connection:
                            this.eventListener.Notify(new CommunicationFailureEvent());
                            break;

                        case CustomFailureType.OutOfPaper:
                            this.eventListener.Notify(new OutOfPaperEvent());
                            break;

                        case CustomFailureType.CoverOpen:
                            this.eventListener.Notify(new CaseOpenFailureEvent());
                            break;

                        case CustomFailureType.Overheating:
                            this.eventListener.Notify(new OverheatingFailureEvent());
                            break;

                        case CustomFailureType.PowerSupply:
                            this.eventListener.Notify(new PowerSupplyFailureEvent());
                            break;

                        case CustomFailureType.PaperJam:
                            this.eventListener.Notify(new PaperJamEvent());
                            break;

                        case CustomFailureType.Cutter:
                            this.eventListener.Notify(new CutterFailureEvent());
                            break;

                        case CustomFailureType.Ram:
                            this.eventListener.Notify(new RamFailureEvent());
                            break;

                        case CustomFailureType.Rom:
                            this.eventListener.Notify(new RomFailureEvent());
                            break;
                    }
                }

                this.stateId++;

                Thread.Sleep(1000);
            }
        }

        public override bool Wait(int time, bool retract)
        {
            if (Available == false)
                return false;

            int startId = this.stateId;
            while (Math.Abs(startId - this.stateId) < 2)
                Thread.Sleep(100);

            this.paperInOutputEvent.WaitOne(Math.Min(time, MaxWaitRetractTime), false);

            return true;
        }

        public override bool Available
        {
            get
            {
                return this.failure == CustomFailureType.None;
            }
        }

        public static bool Supports(string printerName)
        {
            uint error = 0;
            uint count = CePrnInitCeUsbSI(ref error);

            if (error != 0)
                return false;

            int deviceId = 0;
            uint errInt = CePrnGetInterfaceNumUsb(printerName, ref deviceId);
            return errInt == 0;
        }
    }
}
