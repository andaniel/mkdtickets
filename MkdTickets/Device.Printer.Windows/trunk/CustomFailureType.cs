﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Printer.Windows
{
    public enum CustomFailureType
    {
        None,
        Connection,
        OutOfPaper,
        CoverOpen,
        Overheating,
        PowerSupply,
        PaperJam,
        Cutter,
        Ram,
        Rom,
    }
}
