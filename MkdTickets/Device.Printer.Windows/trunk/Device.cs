﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MkdTickets.Printer.Markup;
using MkdTickets.Core.Events;
using MkdTickets.Device.Printer.Events;
using Printer.Markup;

namespace MkdTickets.Device.Printer.Windows
{
    public class Device : IPrinterDevice
    {
        private DeviceConfiguration configuration;
        //private IOriginatedEventListener eventListener;
        private IMonitoring monitoring;

        public Device(DeviceConfiguration configuration, IOriginatedEventListener eventListener)
        {
            this.configuration = configuration;
            //this.eventListener = eventListener;

            switch (this.configuration.MonitoringType)
            {
                case MonitoringType.None:
                    this.monitoring = new VoidMonitoring();
                    break;

                case MonitoringType.Custom:
                    this.monitoring = new CustomMonitoring(eventListener, this.configuration.PrinterName);
                    break;

                default:
                    throw new ArgumentException("Неизвестный тип мониторинга");
            }
        }

        public void Close()
        {
            this.monitoring.Stop();
        }

        public void Print(Document document)
        {
            if (this.monitoring.Available == false)
                throw new PrinterDeviceException("Принтер не работает.");

            DocumentPrinter documentPrinter = new DocumentPrinter(this.configuration, document);
            documentPrinter.Print();
            //this.eventListener.Notify(new ReceiptPrintEvent());
        }

        public bool Wait(int time, bool retract)
        {
            return this.monitoring.Wait(time, retract);
        }

        public IDeviceFactory Factory
        {
            get
            {
                return DeviceFactory.Instance;
            }
        }
    }
}
