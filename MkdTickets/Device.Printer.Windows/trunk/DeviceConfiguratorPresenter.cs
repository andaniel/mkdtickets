﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls;
using System.Drawing.Printing;

using Pioneer.Presentation;

namespace Pioneer.Device.Printer.Windows
{
    public class DeviceConfiguratorPresenter : AbstractPresenter
    {
        private IPresenterContext context;
        private List<CommandBinding> commandBindings = new List<CommandBinding>();

        private ICommand AcceptCommand = new RoutedCommand();
        private ICommand CancelCommand = new RoutedCommand();

        private DeviceConfiguration configuration;
        private DeviceConfiguredHandler configured;

        private DeviceConfiguratorPage page;

        public DeviceConfiguratorPresenter(DeviceConfiguration configuration, DeviceConfiguredHandler configured)
        {
            this.configuration = configuration;
            this.configured = configured;

            this.commandBindings.Add(new CommandBinding(this.AcceptCommand, AcceptExecuted));
            this.commandBindings.Add(new CommandBinding(this.CancelCommand, CancelExecuted));
        }

        public override void Activated()
        {
            page = new DeviceConfiguratorPage();

            page.Acceptor.AcceptCommand = this.AcceptCommand;
            page.Acceptor.CancelCommand = this.CancelCommand;

            foreach (string printerName in PrinterSettings.InstalledPrinters)
                page.PrinterNameComboBox.Items.Add(printerName);

            page.PrinterNameComboBox.SelectedItem = this.configuration.PrinterName;

            page.PrinterNameComboBox.SelectionChanged += new SelectionChangedEventHandler(PrinterNameComboBox_SelectionChanged);

            MonitoringTypeItem[] monitoringItems =
            {
                new MonitoringTypeItem { Name = "Отсутствует", Type = MonitoringType.None },
                new MonitoringTypeItem { Name = "Custom", Type = MonitoringType.Custom },
            };

            MonitoringTypeItem currentMonitoringItem = null;
            foreach (MonitoringTypeItem item in monitoringItems)
            {
                page.MonitoringTypeComboBox.Items.Add(item);
                if (item.Type == this.configuration.MonitoringType)
                    currentMonitoringItem = item;
            }

            if (currentMonitoringItem != null)
                page.MonitoringTypeComboBox.SelectedItem = currentMonitoringItem;

            page.MonitoringTypeComboBox.SelectionChanged += new SelectionChangedEventHandler(MonitoringTypeComboBox_SelectionChanged);

            this.context.Navigate(PageReference.FromMemory(page), this.commandBindings, this.configuration);
        }

        private void PrinterNameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.configuration.PrinterName = (string)page.PrinterNameComboBox.SelectedValue;
        }

        private void MonitoringTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.configuration.MonitoringType = ((MonitoringTypeItem)page.MonitoringTypeComboBox.SelectedValue).Type;
        }

        public override IPresenterContext PresenterContext
        {
            set
            {
                this.context = value;
            }
        }

        private void AcceptExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            this.page.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            this.configured(true);
            this.context.Pop();
        }

        private void CancelExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            this.configured(false);
            this.context.Pop();
        }

        public class MonitoringTypeItem
        {
            public string Name { get; set; }
            public MonitoringType Type { get; set; }

            public override string ToString()
            {
                return this.Name;
            }
        }
    }
}
