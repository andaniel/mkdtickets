﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;


using Printer.Markup;
using MkdTickets.Presentation.Extensions;
using MkdTickets.Printer.Markup;

namespace MkdTickets.Device.Printer.Windows
{
    public class DocumentPrinter
    {
        
        private DeviceConfiguration configuration;
        private Document document;
        private double coef = 1; // коэфицент для переведения мм в пиксели УСТАРЕЛ!!!
        public const PrinterFeature PrinterFeatures = PrinterFeature.None;

        public DocumentPrinter(DeviceConfiguration configuration, Document document)
        {
            this.configuration = configuration;
            this.document = document;
        }

        public void Print()
        {
            try
            {
                PrintDocument printDocument = new PrintDocument();
                if (this.document.AlbumList)
                    printDocument.DefaultPageSettings.Landscape = true;
                printDocument.PrinterSettings.PrinterName = this.configuration.PrinterName;
                printDocument.PrintController = new StandardPrintController();
                printDocument.PrintPage += new PrintPageEventHandler(printDocument_PrintPage);
                //printDocument.DefaultPageSettings.PaperSize = printDocument.PrinterSettings.PaperSizes[2];
                printDocument.Print();
            }
            catch (Exception ex)
            {
                throw new PrinterDeviceException("Ошибка печати документа", ex);
            }
        }

        private Dictionary<string, System.Drawing.Font> ParseFonts()
        {
            Dictionary<string, System.Drawing.Font> fonts = new Dictionary<string, System.Drawing.Font>();

            foreach (KeyValuePair<string, MkdTickets.Printer.Markup.Font> fontDescriptor in this.document.Fonts)
            {
                MkdTickets.Printer.Markup.Font font = fontDescriptor.Value;

                float height = 10;
                switch (font.Size)
                {
                    case FontSize.Small: height = 7; break;
                    case FontSize.Normal: height = 8; break;
                    case FontSize.Big: height = 20; break;
                    case FontSize.VerySmall: height=5; break;
                }

                System.Drawing.FontStyle style = System.Drawing.FontStyle.Regular;
                if ((font.Style & MkdTickets.Printer.Markup.FontStyle.Bold) != 0)
                    style |= System.Drawing.FontStyle.Bold;

                if ((font.Style & MkdTickets.Printer.Markup.FontStyle.Italic) != 0)
                    style |= System.Drawing.FontStyle.Italic;

                if ((font.Style & MkdTickets.Printer.Markup.FontStyle.Underline) != 0)
                    style |= System.Drawing.FontStyle.Underline;

                string face = "Lucida Console";

                System.Drawing.Font systemFont = new System.Drawing.Font(face, height, style);
                fonts.Add(fontDescriptor.Key, systemFont);
            }

            return fonts;
        }

        private void AdvanceBlock(Graphics g, ref int blockHeight, ref int offsetX, ref int offsetY, int height=0)
        {
            offsetX = (int)g.VisibleClipBounds.Left;
            if (height != 0)
                offsetY += height;
            else
                offsetY += blockHeight;
            blockHeight = 0;
        }

        SizeF MeasureStringSize(string text, int offset, int length, Graphics g, System.Drawing.Font font)
        {
            CharacterRange[] characterRanges = { new CharacterRange(0, length + 1), new CharacterRange(length, 1) };

            RectangleF layoutRect = new RectangleF(g.VisibleClipBounds.Left, g.VisibleClipBounds.Top, float.MaxValue, float.MaxValue);

            StringFormat stringFormat = new StringFormat(StringFormatFlags.NoWrap | StringFormatFlags.MeasureTrailingSpaces);
            stringFormat.SetMeasurableCharacterRanges(characterRanges);

            // Measure two ranges in string.
            Region[] stringRegions = g.MeasureCharacterRanges(text.Substring(offset, length) + '.',
                font, layoutRect, stringFormat);

            RectangleF bound1 = stringRegions[0].GetBounds(g);
            RectangleF bound2 = stringRegions[1].GetBounds(g);

            return new SizeF(bound2.Right - bound1.Left, bound1.Height);
        }

        private int MeasureString(string text, int offset, int width, Graphics g, System.Drawing.Font font, out int actualWidth, out int actualHeight)
        {
            int length = 0;
            while (offset + length < text.Length)
            {
                SizeF size = MeasureStringSize(text, offset, length + 1, g, font);
                if (size.Width > width)
                    break;

                length++;
            }

            if (offset + length != text.Length)
            {
                int index = text.LastIndexOf(' ', offset + length, length);
                if (index > offset)
                {
                    length = index - offset;
                }
            }

            string substring = text.Substring(offset, length);

            SizeF actualSize = MeasureStringSize(text, offset, length, g, font);
            actualHeight = (int)Math.Ceiling(actualSize.Height);
            actualWidth = (int)Math.Ceiling(actualSize.Width);

            return length;
        }

        private void DrawText(string text, Graphics g, System.Drawing.Font font, bool isCell, int cellWidth, CellAlignment cellAlignment, ref int blockHeight, ref int offsetX, ref int offsetY)
        {
            bool firstLine = true;
            int offset = 0;
            while (firstLine == true || offset < text.Length)
            {
                if (firstLine == false && text.Length != 0 && text[offset] == ' ')
                    offset++;

                int drawWidth;
                if (firstLine && isCell)
                {
                    drawWidth = cellWidth;
                    if ((int)g.VisibleClipBounds.Width - offsetX < cellWidth)
                        AdvanceBlock(g, ref blockHeight, ref offsetX, ref offsetY);
                }
                else
                {
                    drawWidth = (int)g.VisibleClipBounds.Width - offsetX;
                }

                int width, height;
                int length = MeasureString(text, offset, drawWidth, g, font, out width, out height);
                if (firstLine && isCell && length != text.Length)
                {
                    if (offsetX != g.VisibleClipBounds.Left)
                    {
                        offsetX = (int)g.VisibleClipBounds.Left;
                        offsetY += blockHeight;
                        int reWidth, reHeight;
                        length = MeasureString(text, offset, (int)g.VisibleClipBounds.Width, g, font, out reWidth, out reHeight);
                        width = reWidth;
                        height = reHeight;
                    }
                    else
                    {
                        drawWidth = (int)g.VisibleClipBounds.Width - offsetX;
                        int reWidth, reHeight;
                        length = MeasureString(text, offset, drawWidth, g, font, out reWidth, out reHeight);
                        width = reWidth;
                        height = reHeight;

                        if (length == text.Length)
                            drawWidth = width;
                    }
                }

                string line = text.Substring(offset, length);

                int positionX = offsetX;
                int advanceWidth;

                if (isCell)
                {
                    switch (cellAlignment)
                    {
                        case CellAlignment.Left: positionX = offsetX; break;
                        case CellAlignment.Center: positionX = (drawWidth - width) / 2; break;
                        case CellAlignment.Right: positionX = drawWidth - width; break;
                    }

                    advanceWidth = drawWidth;
                }
                else
                {
                    advanceWidth = width;
                }

                g.DrawString(line, font, Brushes.Black, positionX, offsetY, new StringFormat());

                offsetX += advanceWidth;

                blockHeight = height;

                if (offset + length < text.Length)
                {
                    AdvanceBlock(g, ref blockHeight, ref offsetX, ref offsetY);
                }

                offset += length;

                firstLine = false;
            }
        }


        private void DrawVerticalLine(Graphics g, ref int blockHeight, ref int offsetX, ref int offsetY)
        {
            blockHeight = 8;

            g.FillRectangle(Brushes.Black, offsetX+3, offsetY + 3, 2, g.VisibleClipBounds.Height-6);
        }

        private void DrawCustomRectangle(Graphics g, int blockHeight, int blockWidth, int x, int y, bool showBoard)
        {
            if (showBoard)
            {
                g.DrawRectangle(new Pen(Brushes.Black), Convert.ToInt32(x * coef), Convert.ToInt32(y * coef), Convert.ToInt32(blockWidth * coef), Convert.ToInt32(blockHeight * coef));
            }
        }

        private void DrawRectangle(Graphics g,int blockHeight, int blockWidth, ref int offsetX, ref int offsetY)
        {
            g.DrawRectangle(new Pen(Brushes.Black), offsetX, offsetY, blockWidth, blockHeight);
        }

        private void DrawLine(Graphics g, ref int blockHeight, ref int offsetX, ref int offsetY)
        {
            blockHeight = 8;

            g.FillRectangle(Brushes.Black, g.VisibleClipBounds.Left, offsetY + 3, g.VisibleClipBounds.Width, 2);
        }

        private void DrawFeed(Graphics g, ref int blockHeight, ref int offsetX, ref int offsetY)
        {
            blockHeight = 8;

            g.FillRectangle(Brushes.White, g.VisibleClipBounds.Left, offsetY, g.VisibleClipBounds.Width, blockHeight);
        }

        private void DrawImage(Graphics g, int blockHeight, int blockWidth, int x, int y, Image bitmap)
        {
            RectangleF srcRect = new RectangleF(x, y, blockWidth, blockHeight);
            //GraphicsUnit units = GraphicsUnit.Display;//.Pixel;
            //g.DrawImage(bitmap, new Point(x, y));
            g.DrawImage(bitmap, srcRect);
            //g.DrawImage(bitmap, x, y, srcRect, units);

        }

        private void DrawElement(IDocumentElement element, Graphics g, Dictionary<string, System.Drawing.Font> fonts, ref int blockHeight, ref int offsetX, ref int offsetY, bool useAdvanceBlock=true)
        {
            if (element is IDocumentVariableElement)
            {
                IDocumentVariableElement variableElement = (IDocumentVariableElement)element;

                List<IDocumentElement> elements = variableElement.Resolve(DocumentPrinter.PrinterFeatures);

                foreach (IDocumentElement innerElement in elements)
                    DrawElement(innerElement, g, fonts, ref blockHeight, ref offsetX, ref offsetY);
            }
            else if (element is Text)
            {
                Text text = (Text)element;
                System.Drawing.Font font = fonts[text.Font];
                foreach (object obj in text.Content)
                {
                    if (obj is Cell)
                    {
                        Cell cell = (Cell)obj;
                        int blockWidth = (int)(cell.Width * g.VisibleClipBounds.Width);
                        if (cell.Content is Text)
                        {
                            DrawElement((IDocumentElement)cell.Content, g, fonts, ref blockHeight, ref offsetX, ref offsetY, false);
                        }
                        else
                        {
                            //if (blockHeight ==0)
                            //    DrawRectangle(g,12, blockWidth, ref offsetX, ref offsetY);
                            //else
                            //    DrawRectangle(g, blockHeight, blockWidth, ref offsetX, ref offsetY);
                            DrawText(cell.Content.ToString(), g, font, true, blockWidth, cell.Alignment, ref blockHeight, ref offsetX, ref offsetY);
                        }
                    }
                    else
                    {
                        DrawText(obj.ToString(), g, font, false, 0, CellAlignment.Left, ref blockHeight, ref offsetX, ref offsetY);
                    }
                }
                if (useAdvanceBlock)
                    AdvanceBlock(g, ref blockHeight, ref offsetX, ref offsetY);
            }
            else if (element is MkdTickets.Printer.Markup.Rectangle)
            {
                MkdTickets.Printer.Markup.Rectangle rec = (MkdTickets.Printer.Markup.Rectangle)element;
                coef = Convert.ToDouble(rec.Coef);
                DrawCustomRectangle(g, rec.Height, rec.Width, rec.X, rec.Y, rec.ShowBoard);
                DrawRectangleText((Text)rec.Text, rec.X, rec.Y, rec.Width, rec.Height, fonts, g);
            }
            else if (element is Line)
            {
                DrawLine(g, ref blockHeight, ref offsetX, ref offsetY);

                AdvanceBlock(g, ref blockHeight, ref offsetX, ref offsetY);
            }
            else if (element is VerticalLine)
            {
                DrawVerticalLine(g, ref blockHeight, ref offsetX, ref offsetY);

                AdvanceBlock(g, ref blockHeight, ref offsetX, ref offsetY);
            }
            else if (element is Feed)
            {
                DrawFeed(g, ref blockHeight, ref offsetX, ref offsetY);

                AdvanceBlock(g, ref blockHeight, ref offsetX, ref offsetY);
            }else if(element is ImageMarkup)
            {
                var img = element as ImageMarkup;
                DrawImage(g, img.Height, img.Width, img.X, img.Y, img.Bitmap);
            }
        }


        private void DrawRectangleText(Text text, int floatX, int floatY, int width, int height, Dictionary<string, System.Drawing.Font> fonts, Graphics g)
        {
            string resultString = String.Empty;
            int x = floatX;
            int y = floatY + 5;

            foreach (Cell cell in text.Content)
            {
                if (cell.Alignment == CellAlignment.Center)
                {
                    x = Convert.ToInt32(x + (width - Convert.ToInt32(g.MeasureString(cell.Content.ToString(), fonts[text.Font]).Width)) / 2);
                }
                resultString += cell.Content.ToString(); 
            }
            resultString = resultString.Replace(NewLineForPrinter.NewLineView, "\r\n");
            g.DrawString(resultString, fonts[text.Font], Brushes.Black, new RectangleF((float)(x * coef), (float)(y * coef), (float)(width * coef), (float)(height * coef)));

        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;

            Dictionary<string, System.Drawing.Font> fonts = ParseFonts();
            
            int offsetX = (int)e.Graphics.VisibleClipBounds.Left;
            int offsetY = (int)e.Graphics.VisibleClipBounds.Top;
            int blockHeight = 0;

            foreach (IDocumentElement element in this.document.Elements)
            {
                try
                {
                    DrawElement(element, e.Graphics, fonts, ref blockHeight, ref offsetX, ref offsetY);
                }
                catch 
                {
                    
                }
            }

            e.HasMorePages = false;
        }
    }
}
