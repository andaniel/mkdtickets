﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MkdTickets.Device.Printer.Windows
{
    public class DeviceConfiguration : AbstractDeviceConfiguration, INotifyPropertyChanged
    {
        private string printerName = String.Empty;
        private MonitoringType monitoringType = MonitoringType.None;

        public override string ToString()
        {
            return "Принтер " + this.printerName + " (" + this.MonitoringTypeString + ")";
        }

        public override IDeviceFactory Factory
        {
            get
            {
                return DeviceFactory.Instance;
            }
        }

        public string PrinterName
        {
            get
            {
                return this.printerName;
            }

            set
            {
                if (value != this.printerName)
                {
                    this.printerName = value;
                    NotifyPropertyChanged("PrinterName");
                }
            }
        }

        public MonitoringType MonitoringType
        {
            get
            {
                return this.monitoringType;
            }

            set
            {
                if (value != this.monitoringType)
                {
                    this.monitoringType = value;
                    NotifyPropertyChanged("MonitoringType");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private string MonitoringTypeString
        {
            get
            {
                switch (this.monitoringType)
                {
                    case MonitoringType.Custom: return "custom";
                    case MonitoringType.None: return "без мониторинга";
                    default: return "неизвестно";
                }
            }
        }
    }
}
