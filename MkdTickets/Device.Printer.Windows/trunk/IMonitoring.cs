﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Printer.Windows
{
    public interface IMonitoring
    {
        void Stop();
        bool Wait(int time, bool retract);

        bool Available { get; }
    }
}
