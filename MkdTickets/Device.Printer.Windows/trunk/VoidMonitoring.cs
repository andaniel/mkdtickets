﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MkdTickets.Device.Printer.Windows
{
    public class VoidMonitoring : IMonitoring
    {
        public void Stop()
        {
        }

        public bool Wait(int time, bool retract)
        {
            return false;
        }

        public bool Available
        {
            get
            {
                return true;
            }
        }
    }
}
