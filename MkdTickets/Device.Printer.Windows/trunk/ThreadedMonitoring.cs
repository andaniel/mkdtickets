﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MkdTickets.Device.Printer.Windows
{
    public abstract class ThreadedMonitoring : IMonitoring
    {
        private Thread thread;
        protected bool running;

        protected void Start()
        {
            this.running = true;
            this.thread = new Thread(new ThreadStart(MonitoringLoop));
            this.thread.IsBackground = true;
            this.thread.Start();
        }

        public void Stop()
        {
            if (this.thread != null && this.thread.IsAlive)
            {
                this.running = false;
                this.thread.Join();
                this.thread = null;
            }
        }

        public virtual bool Wait(int time, bool retract)
        {
            return false;
        }

        public abstract void MonitoringLoop();

        public abstract bool Available { get; }
    }
}
