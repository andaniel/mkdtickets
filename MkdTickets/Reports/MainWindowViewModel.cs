﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Reports
{
    class MainWindowViewModel:BaseViewModel
    {
        public string Report { get; set; }
        public  DateTime SelectedDate { get; set; }
        public ICommand GetReportCommand { get; set; }
        public ICommand SaveReportCommand { get; set; }

        
        public MainWindowViewModel()
        {
            SelectedDate = DateTime.Now.Date;
            GetReportCommand = new RelayCommand(GetReportCommandExecuted);
            SaveReportCommand = new RelayCommand(SaveReportCommandExecuted);
        }

        public void GetReportCommandExecuted(object obj)
        {
            var client = new MkdServiceReference.MkdServiceClient();
            Report = client.GetReport(SelectedDate);
            OnPropertyChanged("Report");
        }

        public void SaveReportCommandExecuted(object obj)
        {
            var fi = new FileInfo(SelectedDate.ToString("ddMMyyyy") + ".txt");
            
            if (fi.Exists)
            {
                var result = MessageBox.Show("Файл уже существует. Перезаписать?","Внимание", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }
            using (var streamWriter = fi.CreateText())
            {
                streamWriter.Write(Report);
            }
            MessageBox.Show("Отчет сохранен", "Ура", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
