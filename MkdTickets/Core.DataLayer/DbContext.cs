﻿
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace Core.DataLayer
{
    public class DbContext
    {
        static string dbPath = "C:\\Debug\\Data.db";

        public static bool CheckDbExist()
        {
            var fileInfo = new FileInfo(dbPath);
            return fileInfo.Exists;
        }

        public static void CreateDb()
        {
            if(!CheckDbExist())
            {
                SQLiteConnection.CreateFile(dbPath);

                using (var connection = new SQLiteConnection("Data Source="+dbPath+ ";Version=3;"))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand("CREATE TABLE \"Tickets\" (\"Id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\"DateTime\"  TEXT NOT NULL,\"Type\"  TEXT NOT NULL, \"Code\" TEXT NOT NULL, \"IsTest\" INTEGER NOT NULL); ",connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    using (var command = new SQLiteCommand("CREATE TABLE \"Logo\" (\"Id\" INTEGER NOT NULL,\"DateTime\"  TEXT NOT NULL); ", connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void InsertTicket(TicketType type, string code, int isTest)
        {
            using (var connection = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                connection.Open();

                using (var command = new SQLiteCommand("INSERT INTO \"Tickets\" (DateTime,Type,Code,IsTest) VALUES (@DateTime,@Type,@Code,@IsTest); ", connection))
                {
                    command.Parameters.AddWithValue("@DateTime", DateTime.Now);
                    command.Parameters.AddWithValue("@Type", type.ToString());
                    command.Parameters.AddWithValue("@Code", code);
                    command.Parameters.AddWithValue("@IsTest", isTest);
                    command.ExecuteNonQuery();
                }
            }
        }
        public static int CodeTicketCount(string code)
        {
            using (var connection = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                connection.Open();

                using (var command = new SQLiteCommand("SELECT COUNT(*) FROM \"Tickets\"  WHERE DateTime > @startdatetime AND DateTime < @stopdatetime AND Code = @Code; ", connection))
                {
                    command.Parameters.AddWithValue("@startdatetime", DateTime.Now.Date);
                    command.Parameters.AddWithValue("@stopdatetime", DateTime.Now.AddDays(1).Date);
                    command.Parameters.AddWithValue("@Code", code);
                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
        }
        public static int GetTicketsCount(TicketType type, DateTime start, DateTime stop)
        {
            using (var connection = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                connection.Open();

                using (var command = new SQLiteCommand("SELECT Count(*) FROM \"Tickets\" WHERE Type=@Type AND DateTime > @startdatetime AND DateTime < @stopdatetime AND IsTest = 0; ", connection))
                {   
                    command.Parameters.AddWithValue("@Type", type.ToString());
                    command.Parameters.AddWithValue("@startdatetime", start);
                    command.Parameters.AddWithValue("@stopdatetime", stop);
                    var result = Convert.ToInt32(command.ExecuteScalar());
                    return result;
                }
            }
        }

        public static int GetLogoId()
        {
            using (var connection = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                connection.Open();

                using (var command = new SQLiteCommand("SELECT Id FROM \"Logo\" ; ", connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var result = Convert.ToInt32(reader["Id"]);
                            return result;
                        }
                        return 1;
                    }
                    
                }
            }
        }

        public static void SaveLogoId(int id)
        {
            using (var connection = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                connection.Open();
                using (var command = new SQLiteCommand("DELETE FROM \"Logo\" ", connection))
                {
                    command.ExecuteNonQuery();
                }
                    using (var command = new SQLiteCommand("INSERT INTO \"Logo\" (Id,DateTime) VALUES (@Id,@DateTime); ", connection))
                {
                    command.Parameters.AddWithValue("@Id", id);
                    command.Parameters.AddWithValue("@DateTime", DateTime.Now);
                    command.ExecuteNonQuery();
                }

            }
        }

        public static DateTime GetLogoDate()
        {
            using (var connection = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                connection.Open();

                using (var command = new SQLiteCommand("SELECT DateTime FROM \"Logo\" ; ", connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var result = Convert.ToDateTime(reader["DateTime"]);
                            return result;
                        }
                        return DateTime.MinValue;
                    }

                }
            }
        }

    }
}
