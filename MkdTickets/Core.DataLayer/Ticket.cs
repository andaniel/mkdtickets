﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataLayer
{
    public class Ticket
    {
        
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Type { get; set; }
    }
}
