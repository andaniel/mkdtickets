﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataLayer
{
    public enum TicketType
    {
        Full,
        Half,
        Tourist,
        ChildFull,
        ChildHalf,
        ChildTourist
    }
}
