﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using TicketChecker.Views;
using Xamarin.Forms;

namespace TicketChecker.ViewModels
{
	public class MainPageViewModel : BaseViewModel 
    {
		public string SelectedBthDevice { get; set; }// = "1560PCJ11930000106";

		int _sleepTime = 250;
		Color bColor;
		private string message;
		public string Message
		{
			get
			{
				return message;
			}
			set
			{
				message = value;
				OnPropertyChanged("Message");
			}
		}
		
		public Color BackgroundColor { get
			{
				return bColor;
			}
			set
			{
				bColor = value;
				OnPropertyChanged("BackgroundColor");
			}
		}
		public Command SettingsCommand { get; set; }
		public MainPageViewModel()
        {
			
			SelectedBthDevice = (string)Application.Current.Properties["BtDevice"];
			
			SettingsCommand = new Command(()=> {
				var p = new SettingsPage();
				p.BindingContext = new SettingsPageViewModel();
				App.Current.MainPage.Navigation.PushAsync(p);
			});

			MessagingCenter.Subscribe<App>(this, "Sleep", (obj) =>
			{
				// When the app "sleep", I close the connection with bluetooth
				//if(_isConnected)
				DependencyService.Get<IBtScaner>().Cancel();

			});

			MessagingCenter.Subscribe<App>(this, "Resume", (obj) =>
			{
					DependencyService.Get<IBtScaner>().Start(SelectedBthDevice, _sleepTime, true);
			});

			MessagingCenter.Subscribe<App,string>(this, "Barcode", async (sender,arg) =>
			 {
				 arg = arg.Trim();
				 string result;
				 try
				 {
					 MkdServiceReference.MkdServiceClient client = new MkdServiceReference.MkdServiceClient();
					 result = client.Check(arg);
				 }catch(Exception ex)
                 {
					 result = "Ошибка связи с сервером";
                 }
				 switch (result)
				 {
					 case "Ошибка связи с сервером":
						 Message = result;
						 BackgroundColor = Color.BlueViolet;
						 break;
					 case "No Ticket":
						 Message = "Плохой билет";
						 BackgroundColor = Color.Red;
						 PlayAlarmAudio();
						 break;
					 case "0":
						 Message = "Дневной Ок";
						 BackgroundColor = Color.LightGreen;
						 PlayCorrectAudio();
						 break;
					 case "1":
						 Message = "Полдня Ок";
						 BackgroundColor = Color.LightGreen;
						 PlayCorrectAudio();
						 break;
					 case "2":
						 Message = "Туристический Ок";
						 BackgroundColor = Color.LightGreen;
						 PlayCorrectAudio();
						 break;
					 case "3":
						 Message = "Детский Дневной Ок";
						 BackgroundColor = Color.Yellow;
						 PlayCorrectAudio();
						 break;
					 case "4":
						 Message = "Детский Полдня Ок";
						 BackgroundColor = Color.Yellow;
						 PlayCorrectAudio();
						 break;
					 case "5":
						 Message = "Детский Туристический Ок";
						 BackgroundColor = Color.Yellow;
						 PlayCorrectAudio();
						 break;

					 case "FastPass":
						 Message = "Слишком быстро";
						 BackgroundColor = Color.Red;
						 PlayAlarmAudio();
						 break;
					 default:
						 break;
				 }
				 Thread.Sleep(2000);
				 Message = "Отсканируйте билет";
				 BackgroundColor = Color.Transparent;
			 });

			MessagingCenter.Subscribe<App, string>(this, "SelectedDevice", async (sender, arg) =>
			{
				DependencyService.Get<IBtScaner>().Cancel();
				SelectedBthDevice = arg;
				DependencyService.Get<IBtScaner>().Start(SelectedBthDevice, _sleepTime, true);
			});
			DependencyService.Get<IBtScaner>().Start(SelectedBthDevice, _sleepTime, true);

			Message = "Отсканируйте билет";
		}

		private void PlayAlarmAudio()
		{
			try
			{
				var stream = GetStreamFromFile("alarm.mp3");
				var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
				audio.Load(stream);
				audio.Play();
			}
			catch (Exception e)
			{
				//Debug.WriteLine("exception:>>" + e);
			}
		}

		private void PlayCorrectAudio()
		{
			try
			{
				var stream = GetStreamFromFile("correct.mp3");
				var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
				audio.Load(stream);
				audio.Play();
			}
			catch (Exception e)
			{
				//Debug.WriteLine("exception:>>" + e);
			}
		}
		Stream GetStreamFromFile(string filename)
		{
			var assembly = typeof(App).GetTypeInfo().Assembly;

			var stream = assembly.GetManifestResourceStream("TicketChecker." + filename);

			return stream;
		}
	}
}
