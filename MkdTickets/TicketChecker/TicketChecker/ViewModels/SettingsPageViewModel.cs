﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace TicketChecker.ViewModels
{
    public class SettingsPageViewModel:BaseViewModel
    {
        public ObservableCollection<string> PairedDevices { get; set; }
        public string SelectedDevice { get; set; }
        public Command SaveCommand { get; set; }
        public SettingsPageViewModel()
        {
            PairedDevices = DependencyService.Get<IBtScaner>().PairedDevices();
            

            SaveCommand = new Command(() => {
                Application.Current.Properties["BtDevice"] = SelectedDevice;
                Application.Current.SavePropertiesAsync();
                MessagingCenter.Send<App, string>((App)Xamarin.Forms.Application.Current,"SelectedDevice", SelectedDevice);
                var stack = Application.Current.MainPage.Navigation.PopToRootAsync();
            });

        }
    }
}
