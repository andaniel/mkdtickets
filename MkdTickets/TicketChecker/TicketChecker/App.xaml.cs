﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using TicketChecker.Views;
using TicketChecker.ViewModels;

namespace TicketChecker
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            if (!Application.Current.Properties.ContainsKey("BtDevice"))
            {
                Application.Current.Properties.Add("BtDevice", "1560PCJ11930000106");
                Application.Current.SavePropertiesAsync();

            }

            

            var mainPageContext = new MainPageViewModel();
            MainPage = new NavigationPage(new MainPage());
            MainPage.BindingContext = mainPageContext;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
            MessagingCenter.Send<App>(this, "Sleep");
        }

        protected override void OnResume()
        {
            MessagingCenter.Send<App>(this, "Resume");
        }
    }
}
