﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace BarcodeScaner.Configuration
{
    public class BarcodeScanerConfiguration:ConfigurationSection
    {
        public static BarcodeScanerConfiguration Current
        {
            get { return ConfigurationManager.GetSection("BarcodeScaner") as BarcodeScanerConfiguration; }
        }

        [ConfigurationProperty("Port", IsRequired = true)]
        public string Port
        {
            get
            {
                return (string)this["Port"];
            }
        }

    }
}
