﻿using Core.DataLayer;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace BarcodeScaner
{
    public class Scaner:IDisposable
    {
        SerialPort port;
        public Scaner()
        {
            port = new SerialPort(Configuration.BarcodeScanerConfiguration.Current.Port);
            port.DataReceived += Port_DataReceived;
            port.Open();
        }
        //переоткрывать порт каждую секунду
        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var sp = sender as SerialPort;
            var code = sp.ReadExisting();

            var ticketCount = DbContext.CodeTicketCount(code);

            if(ticketCount == 0)
            {
                var bufferContinue = new byte[] { 0x23, 0x40, 0x2e, 0x2e, 0x2e, 0x2e, 0x0d };
                sp.Write(bufferContinue, 0, bufferContinue.Length);
            }
            if(ticketCount>0)
            {
                var bufferAck = new byte[] { 0x06 };
                var bufferBeep = new byte[] { 0x23, 0x40, 0x2f, 0x2f, 0x2f, 0x2f, 0x0d };
                sp.Write(bufferBeep, 0, bufferBeep.Length);
                sp.Write(bufferAck, 0, bufferAck.Length);
                
            }
            //var bufferAck = new byte[] { 0x06 };
            //var bufferNak = new byte[] { 0x15 };
            ////var bufferContinue = new byte[] { 0x23, 0x40, 0x2e, 0x2e, 0x2e, 0x2e, 0x0d };
            //var bufferBeep = new byte[] { 0x23, 0x40, 0x2f, 0x2f, 0x2f, 0x2f, 0x0d };
            ////sp.Write(bufferContinue, 0, bufferContinue.Length);
            //sp.Write(bufferBeep, 0, bufferBeep.Length);
            //sp.Write(bufferAck, 0, bufferAck.Length);
        }

        public void Dispose()
        {
            port.Close();
        }
    }
}
